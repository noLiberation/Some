import javax.swing.JOptionPane;

/*
Program:    GuessingGame
Programmer:     Alan Shepherd
Date:   4-19-2017
Description: Lab 8
*/

public class GuessingGame 
{

 
    public static void main(String[] args) 
    {
        int count = 0;
        int userGuess = 0;
        int randomNumber = (int) (Math.random() * 100 + 1);
        System.out.println("The correct guess would be " + randomNumber);
        
        
        do
        {
            String response = JOptionPane.showInputDialog(null,
                    "Enter a guess between 1 and 100", "Guessing Game", 3);
            userGuess = Integer.parseInt(response);
            count = count +1;
            String guessResponse = determineGuess(userGuess, randomNumber);
            
            String responseString = "your guess is " + guessResponse + ".";
                responseString += "\nTry number: " + count; 
                JOptionPane.showMessageDialog(null, responseString);
        }
        while (userGuess != randomNumber);
    }
    public static String determineGuess(int userAnswer, int computerNumber)
    {
        String feedback = "";
        
        if (userAnswer <=0 || userAnswer > 100)
            feedback = "invalid";
        else if (userAnswer == computerNumber)
            feedback = "Correct";
        // Too Low
        if (userAnswer <=0)
            feedback = "invalid";
        
        // Too High
        if (userAnswer >=100)
            feedback = "invalid";
        return feedback;
        
        
    }
    
}